# @chiwaukee/semantic-release-config

## Usage

```shell
npm i --save-dev @chiwaukee/semantic-release-config
```

in `.releaserc`:

```json
{
  "extends": "@chiwaukee/semantic-release-config"
}
```