## [1.5.2](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.5.1...v1.5.2) (2022-08-27)


### Bug Fixes

* include index.js ([5841a33](https://gitlab.com/chiwaukee/semantic-release-config/commit/5841a335c5d266c03adcff4ec5ce607be77e8611))

## [1.5.1](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.5.0...v1.5.1) (2022-08-27)


### Bug Fixes

* set presets ([9700081](https://gitlab.com/chiwaukee/semantic-release-config/commit/9700081a3f1193953862752df4e58e3e5909b726))

# [1.5.0](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.4.0...v1.5.0) (2022-08-26)


### Features

* update readme ([3ba0fae](https://gitlab.com/chiwaukee/semantic-release-config/commit/3ba0fae56847a54542675021ac442ede17523144))

# [1.4.0](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.3.0...v1.4.0) (2022-08-26)


### Features

* fix npm config ([e200a6a](https://gitlab.com/chiwaukee/semantic-release-config/commit/e200a6a1ccccb2a7e6bd7a704dc32aadb5a4bdc8))

# [1.3.0](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.2.0...v1.3.0) (2022-08-26)


### Features

* use actual config ([fe5eac3](https://gitlab.com/chiwaukee/semantic-release-config/commit/fe5eac3f68e27cfa9711fd8882287912b9594c1d))

# [1.2.0](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.1.0...v1.2.0) (2022-08-26)


### Features

* use actual config ([e7bcc88](https://gitlab.com/chiwaukee/semantic-release-config/commit/e7bcc88e29f7f807424a6cf150a45876336ad169))

# [1.1.0](https://gitlab.com/chiwaukee/semantic-release-config/compare/v1.0.0...v1.1.0) (2022-08-26)


### Features

* use actual config ([99e8d03](https://gitlab.com/chiwaukee/semantic-release-config/commit/99e8d03a37325abca63c780441938ed68ce50164))
* use actual config ([2e764d8](https://gitlab.com/chiwaukee/semantic-release-config/commit/2e764d81c22dec08b31572a07cfdf9798f37e9b7))
* use self as config ([e68ee7a](https://gitlab.com/chiwaukee/semantic-release-config/commit/e68ee7a9d5884996aafe45ce2fe0ed68e44314d2))

# 1.0.0 (2022-08-26)


### Features

* initial commit ([7423c81](https://gitlab.com/chiwaukee/semantic-release-config/commit/7423c8197c9466916fef99e707bf596b4a379787))

## [1.1.3](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.2...v1.1.3) (2022-08-26)


### Bug Fixes

* npm publish with ci ([ea68cf8](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/ea68cf808dc122eb2b77759f00e6cd33c71ccc77))

## [1.1.2](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.1...v1.1.2) (2022-08-26)


### Bug Fixes

* npm publish with ci ([2ae3541](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/2ae35415d021e828bd88b68953a4f64dbaa8dbeb))

## [1.1.1](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.1.0...v1.1.1) (2022-08-26)


### Bug Fixes

* npm publish with ci ([1ddca00](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/1ddca002b91f29dfd1a651d18021ce95f70f28a6))

# [1.1.0](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/compare/v1.0.0...v1.1.0) (2022-08-26)


### Bug Fixes

* node image ([ed6bfeb](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/ed6bfeb018abe5b9a949ca1fa3e78034e55b60fc))


### Features

* add publish step ([be8a985](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/be8a98535c327ede190f33196fb68971867adfe5))
* add publish step ([93f4c21](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/93f4c21a203db52d73a8ea219f2a16157773135d))
* fix npm release ([b7e3b46](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/b7e3b46d50f93e40dd604474e6199a9f22da979f))

# 1.0.0 (2022-08-26)


### Features

* add commitlint ([75bf411](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/75bf411742f1a3aa2149b2f47f214ef6fa1fc899))
* initial commit ([18b9b77](https://gitlab.com/chiwaukee/chiwaukee-eslint-config/commit/18b9b77492016c27d930ede21d2cb4e611b6d352))
